package ru.yafasov.hw09;

public class Ellipse extends Figure {
    private double pi=3.14;
    private int r1;
    private int r2;

    public Ellipse(int r1, int r2) {
        this.r1 = r1;
        this.r2 = r2;
    }

    public double getPerimeter() {
        return 4 * ((pi * r1 * r2 + (r1 - r2)) / (r1 + r2));
    }
}
