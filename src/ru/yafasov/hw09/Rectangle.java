package ru.yafasov.hw09;

public class Rectangle extends Figure{
    private int a;
    private int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }
    public double getPerimeter() {
        return (a+b)*2;
    }
}
