/*
Сделать класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть метод getPerimeter(), который возвращает 0.
Во всех остальных классах он должен возвращать корректное значение.
 */

package ru.yafasov.hw09;

public class Task9 {
    public static void main(String[] args) {
       Figure figure = new Figure();
       Ellipse ellipse = new Ellipse(5, 3);
       Rectangle rectangle = new Rectangle(10, 20);
       Circle circle = new Circle(8);
       Square square = new Square(11);

        System.out.println("Периметр фигуры " + figure.getPerimeter());
        System.out.println("Периметр эллипса " + ellipse.getPerimeter());
        System.out.println("Периметр прямоугольника " + rectangle.getPerimeter());
        System.out.println("Периметр окружности " + circle.getPerimeter());
        System.out.println("Периметр квадрата " + square.getPerimeter());
    }
}

