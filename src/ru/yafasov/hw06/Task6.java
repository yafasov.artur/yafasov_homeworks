/*
Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
стало
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
*/
package ru.yafasov.hw06;

import java.util.Arrays;

public class Task6 {
    public static void main(String[] args) {
        int[] arr = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int result = findIndex(arr,20);
        System.out.println("Индекс заданного числа(20) в массиве = " + result);

        replaceIndex(arr);
    }
    public static int findIndex(int[] arrayLocal, int value) {
        int index = -1;

        for (int i = 0; i < arrayLocal.length; i++) {
            if (value == arrayLocal[i]) {
                index = i;
            }
        }
        return index;
    }
    public static void replaceIndex(int[] arrayReplace) {

        for (int i = 0; i < arrayReplace.length; i++) {
            if (arrayReplace[i] == 0) {
                for (int j = i; j < arrayReplace.length; j++) {
                    if (arrayReplace[j] != 0) {
                        arrayReplace[i] = arrayReplace[j];
                        arrayReplace[j] = 0;
                        break;
                    }
                }
            }
        }
        System.out.println("Отсортированый массив = " + Arrays.toString(arrayReplace));
    }
}

