package ru.yafasov.attestation01;

import java.util.Map;

interface UsersRepository {
    Map<Integer, User> findAll();
    void addNewUserRecord(String userName, int userAge, boolean isWorked);
    void findUserByAge(int age);
    User findById(int id);
    void update(User user);
}
