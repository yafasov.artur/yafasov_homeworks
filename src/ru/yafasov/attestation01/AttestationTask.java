/*
Реализовать в классе UsersRepositoryFileImpl методы:

User findById(int id);
update(User user);

Принцип работы методов:
Пусть в файле есть запись:
1|Игорь|33|true

Где первое значение - гарантированно уникальный ID пользователя (целое число).
Тогда findById(1) вернет объект user с данными указанной строки.

Далее, для этого объекта можно выполнить следующий код:
user.setName("Марсель");
user.setAge(27);
и выполнить update(user);

При этом в файле строка будет заменена на 1|Марсель|27|true.
Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.

Примечания:
Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)
*/
package ru.yafasov.attestation01;

public class AttestationTask {
    public static void main(String[] args) {
        UsersRepository repository = new UsersRepositoryFile("src/ru/yafasov/attestation01/data.txt");
//        var foundUser = repository.findAll();
        repository.findAll(); // Отобразить всех пользователей

//        repository.addNewUserRecord("Marsel", 42, true);
        repository.findUserByAge(27);

        var foundUser = repository.findById(4); // Поиск пользователя с ID 3
        if (foundUser != null) {
            foundUser.setUserAge(80);
            foundUser.setUserName("Сидоров");
            System.out.println("Замена данных у пользователя ");
            repository.update(foundUser);
            repository.findAll(); // Отобразить всех пользователей
        } else {
            System.out.println("Несуществующий ID пользователя");
        }
    }
}

