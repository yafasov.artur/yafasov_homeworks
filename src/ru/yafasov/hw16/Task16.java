/*
Реализовать removeAt(int index) для ArrayList
Реализовать метод T get(int index) для LinkedList
 */

package ru.yafasov.hw16;

import java.util.LinkedList;

public class Task16 {
    public static void main(String[] args) {
        System.out.println("Реализация ArrayList");

        ArrayList<Integer> array = new ArrayList<>();

        array.add(12);
        array.add(21);
        array.add(555);
        array.add(56);
        array.add(888);
        array.add(42);
        array.add(90);
        array.print(); // Вывод массива со всеми элементами

        array.removeAt(4); // Удаление элемент с индексом 4 (888)
        array.print(); // Вывод обновленного массива после удаления элемента

        System.out.println("Реализация LinkedList");

        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(12);
        linkedList.add(21);
        linkedList.add(555);
        linkedList.add(56);
        linkedList.add(888);
        linkedList.add(42);
        linkedList.add(90);

        System.out.println("LinkedList.get(5) -> " + linkedList.get(5)); // 42
    }
}

