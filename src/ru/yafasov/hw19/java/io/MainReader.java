package ru.yafasov.hw19.java.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/*
 * Считывает символы.
 * */

public class MainReader {
    public static void main(String[] args) {
        try {
            Reader reader = new FileReader("file.txt");
            var currentCharacter = reader.read();

            while (currentCharacter != -1) {
                System.out.print((char) currentCharacter);
                currentCharacter = reader.read();
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}