package ru.yafasov.hw19.java.io;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/*
 * Предназначение: Работа со строками, с текстом на прямую.
 * */

public class MainWriter {
    public static void main(String[] args) {
        try {
            Writer writer = new FileWriter("file.txt");
            writer.write("Hello from File!");
            writer.close();
        } catch (IOException e) {
            System.out.println("Что-то не так - " + e.getMessage());
        }
    }
}
