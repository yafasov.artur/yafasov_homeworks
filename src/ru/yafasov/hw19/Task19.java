/*
Реализовать методы UsersRepository
 */
package ru.yafasov.hw19;

import ru.yafasov.hw19.repository.User;
import ru.yafasov.hw19.repository.UserRepository;
import ru.yafasov.hw19.repository.UserRepositoryFileImpl;

import java.util.List;

public class Task19 {
    public static void main(String[] args) {
        UserRepository allRecords = new UserRepositoryFileImpl("src/ru/yafasov/hw19/users.txt");
        List<User> users = allRecords.findAll();

        System.out.println("\nФорматированный вывод пользователей:");
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorked());
        }

        System.out.println("\nПользователи с возрастом == 36:");
        UserRepository searchUserFromAge = new UserRepositoryFileImpl("src/ru/yafasov/hw19/users.txt");
        List<User> foundByAge = searchUserFromAge.findByAge(36);
        for (User user :
                foundByAge) {
            System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorked());
        }

        System.out.println("\nРаботающие пользователи:");
        UserRepository searchUserIsWorked = new UserRepositoryFileImpl("src/ru/yafasov/hw19/users.txt");
        List<User> foundIsWorked = searchUserIsWorked.findAllWorkers();
        for (User user :
                foundIsWorked) {
            System.out.println(user.getName() + " " + user.getAge() + " " + user.isWorked());
        }
    }
}