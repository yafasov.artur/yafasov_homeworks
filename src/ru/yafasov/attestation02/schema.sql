create SCHEMA attestation02;

-- Создание таблицы для товаров
create TABLE attestation02.product (
                                       id integer NOT NULL PRIMARY KEY,
                                       name varchar(40) NOT NULL,
                                       price integer NOT NULL,
                                       quantity integer NOT NULL
);

-- Создание таблицы для заказчиков
create TABLE attestation02.customer (
                                        id integer NOT NULL PRIMARY KEY,
                                        name varchar(40) NOT NULL
);

--Создание таблицы для заказов
create TABLE attestation02.orders (
                                      id_product integer NOT NULL,
                                      foreign key (id_product) references attestation02.product(id),
                                      id_customer integer NOT NULL ,
                                      foreign key (id_customer) references attestation02.customer(id),
                                      date varchar(40) NOT NULL,
                                      quantity integer NOT NULL
);

-- Вывод всех заказчиков
select *
from attestation02.customer;

-- Вывод имени заказчика под id = 1
select attestation02.customer.name
from attestation02.customer
where attestation02.customer.id = 1;

-- Вывод всех заказов заказчиков
select *
from attestation02.customer tc
         join attestation02.orders od
              on tc.id = od.id_customer;


-- Вывод максимальной цены товара
select max(attestation02.product.price)
from attestation02.product;

-- Добавление нового заказчика
insert into attestation02.customer(id, name)
values (4, 'Anton');

-- Внесение изменений в таблицу заказчика
update attestation02.customer
set name = 'Maximus'
where id = 3;