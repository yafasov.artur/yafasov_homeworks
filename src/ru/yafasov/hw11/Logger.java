package ru.yafasov.hw11;

public class Logger {
    private int i = 0;
    private static Logger loggerInstance;

    public Logger() {
    }

    public static synchronized Logger getLogger() {
        if (null == loggerInstance) {
            loggerInstance = new Logger();
            System.out.println("Creating new instance");
        }
        return loggerInstance;
    }

    public void logMessage() {
        System.out.println("Не особо понял как это работает, но думаю со временем разберусь " + i++);
    }
}
