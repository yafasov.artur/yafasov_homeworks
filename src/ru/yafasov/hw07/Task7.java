package ru.yafasov.hw07;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Task7 {
    public static void main(String[] args) {
        int[] arr = new int[]{55, 7, -88, 7, 55, 23, 5, -9, 76, -88, 55, 5, -9, 76, 5}; // return 23

            int[] result = new int[201];

            for (int j : arr) {
                result[j + 100]++;
            }

            int minIndex = result.length;
            int minValue = Integer.MAX_VALUE;
            for (int i = 0; i < result.length; i++) {
                if (result[i] > 0 && result[i] < minValue) {
                    minValue = result[i];
                    minIndex = i;
                }
            }
            System.out.println(Arrays.toString(result));
            System.out.println(minIndex - 100);

            findIndex(arr);
        }
        private static void findIndex ( int[] arr){
            Map<Integer, Integer> arrMap = new HashMap<>();
            for (int j : arr) {
                if (arrMap.containsKey(j)) {
                    arrMap.put(j, arrMap.get(j) + 1);
                } else {
                    arrMap.put(j, 1);
                }
            }

            int minValue = Integer.MAX_VALUE;
            int minKey = Integer.MAX_VALUE;
            for (Integer key : arrMap.keySet()) {
                if (arrMap.get(key) < minValue) {
                    minKey = key;
                    minValue = arrMap.get(key);
                }
            }
            System.out.println(minKey);
        }
    }






