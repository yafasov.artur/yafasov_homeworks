/*
На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово
в этой строке.
Вывести:
Слово - количество раз
Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы,
ограниченные пробелами справа и слева.
 */
package ru.yafasov.hw17;

import java.util.Arrays;
import java.util.List;

import static ru.yafasov.hw17.CounterOfWords.counterOfWords;
import static ru.yafasov.hw17.Repository.readFile;

public class Task17 {
    public static void main(String[] args) {
        String str = readFile();
        System.out.println("\nИсходная строка: " + str + "\n");
        assert str != null;
        String text = str.replaceAll(",", ""); // Удаление лишних запятых

        System.out.println("Replace: " + text + "\n");

        List<String> textArray = Arrays.stream(text.split(" ")).toList();
        System.out.println("Количество слов: " + textArray.size() + "\n");

        System.out.println("Слово - Количество повторений");
        counterOfWords(textArray);
    }
}
