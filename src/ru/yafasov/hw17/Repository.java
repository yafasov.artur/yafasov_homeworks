package ru.yafasov.hw17;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Repository {
    public static String readFile() {
        String line = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("src/ru/yafasov/hw17/Text.md"));
            while ((line = bufferedReader.readLine()) != null) {
                return line;
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }
}
