package ru.yafasov.hw17;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CounterOfWords {
    protected static void counterOfWords(List arr) {
        Map<String, Integer> arrMap = new HashMap<>();
        for (Object o : arr) {
            if (arrMap.containsKey(o)) {
                arrMap.put((String) o, arrMap.get(o) + 1);
            } else {
                arrMap.put((String) o, 1);
            }
        }

        for (String word :
                arrMap.keySet()) {
            String value = arrMap.get(word).toString();
            System.out.println(word + " - " + value);
        }
    }
}