package ru.yafasov.hw10;

public class Circle extends Figure implements MoveFigure {

    public Circle(int x, int y) {
        super(x, y);
    }
    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}