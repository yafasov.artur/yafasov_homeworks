package ru.yafasov.hw10;

public class Square extends Figure implements MoveFigure {

    public Square(int x, int y) {
        super(x, y);
    }
    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}