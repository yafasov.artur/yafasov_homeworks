/*Сделать класс Figure из задания 09 абстрактным.
Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
*/
package ru.yafasov.hw10;

public class Task10 {

    public static void main(String[] args) {
        MoveFigure[] moveFigures = new MoveFigure[2];
        Circle circle = new Circle(10, 15);
        Square square = new Square(-10, -15);

        moveFigures[0] = circle;
        moveFigures[1] = square;

        System.out.print("Круг: ");
        circle.showXY();
        System.out.print("Квадрат: ");
        square.showXY();

        for (MoveFigure moveFigure : moveFigures) {
            moveFigure.move(0, 0);
        }

        System.out.print("Круг: ");
        circle.showXY();
        System.out.print("Квадрат: ");
        square.showXY();
    }
}
