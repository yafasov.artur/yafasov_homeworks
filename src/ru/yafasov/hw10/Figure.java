package ru.yafasov.hw10;

public abstract class Figure {
    public int x;
    public int y;

    public Figure(int x, int y){
        this.x = x;
        this.y = y;
    }
    public int getX(){
        return this.x;
    }
    public int getY(){
        return  this.y;
    }
    protected void setX(int x){
        this.x = x;
    }
    protected void setY(int y){
        this.y = y;
    }
    public void showXY(){

        System.out.println("X -> " + getX());
        System.out.println("Y -> " + getY());
    }
}
