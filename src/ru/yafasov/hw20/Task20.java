/*
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
o001aa111|Camry|Black|133|800
o002aa111|Camry|White|0|130
o001aa111|Camry|Black|133|790
o003aa111|Camry|Red|133|700
o004aa111|Camry|Gray|133|810

Используя Java Stream API, вывести:
Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry *
https://habr.com/ru/company/luxoft/blog/270383/
 */
package ru.yafasov.hw20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Task20 {
    public static void main(String[] args) {

        Map<String, Car> map = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("src/ru/yafasov/hw20/inputData.txt"))) {
            String readLine = reader.readLine();

            while (readLine != null) {

                String[] split = readLine.split("\\|");
                map.put(split[0], new Car(split[0], split[1], split[2], Integer.parseInt(split[3]), Double.parseDouble(split[4])));

                readLine = reader.readLine();
            }

            // Чтение данных из файла
            System.out.println("Чтение данных из файла:");
            map.values().forEach(System.out::println);

            // Filter -> Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
            List<Map.Entry<String, Car>> findFromOdometer = map.entrySet().stream()
                    .filter(v -> v.getValue().getColor().equals("Black") ||
                            v.getValue().getCarOdometer() == 0)
                    .toList();

            System.out.println("\nФильтр [черный цвет или нулевой пробег]: ");
            findFromOdometer.forEach(i -> System.out.println(i.getValue().getCarID()));

            // Filter ->  Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
            long findFromUniq = map.entrySet().stream()
                    .filter(v -> v.getValue().getCarCost() >= 700 &&
                            v.getValue().getCarCost() <= 800)
                    .count();

            System.out.println("\nФильтр [кол-во уникальных автомобилей от 700 до 800 тыс]: ");
            System.out.println(findFromUniq);

            // Filter -> * Вывести цвет автомобиля с минимальной стоимостью. // min + map
            List<String> findFromColor = map.values().stream()
                    .min(Comparator.comparing(Car::getCarCost))
                    .stream()
                    .map(Car::getColor)
                    .toList();

            System.out.println("\nФильтр [цвет автомобиля с минимальной стоимостью]:");
            System.out.println(findFromColor.get(0));

            // Filter -> Среднюю стоимость Camry
            // System.out.println((810 + 700 + 130 + 790) / 4); // 607
            var foundMiddleCost = map.values().stream()
                    .mapToDouble(Car::getCarCost).average().getAsDouble();


            System.out.println("\nФильтр [средняя стоимость Camry]: ");
            System.out.println(foundMiddleCost);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}