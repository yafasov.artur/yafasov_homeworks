package ru.yafasov.hw20;

import java.util.Map;

public class Car {
    private String carID;
    private String carModel;
    private String color;
    private int carOdometer;
    private double carCost;

    public Car(String carID, String carModel, String color, int carOdometer, double carCost) {
        this.carID = carID;
        this.carModel = carModel;
        this.color = color;
        this.carOdometer = carOdometer;
        this.carCost = carCost;
    }

    @Override
    public String toString() {
        return carID + " | " + carModel + " | " + color + " | " + carOdometer + " | " + carCost;
    }

    public String getCarID() {
        return carID;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCarOdometer() {
        return carOdometer;
    }

    public void setCarOdometer(int carOdometer) {
        this.carOdometer = carOdometer;
    }

    public double getCarCost() {
        return carCost;
    }

    public void setCarCost(double carCost) {
        this.carCost = carCost;
    }
}

